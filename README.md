# General information

This project contains the selections for the Stripping campaigns. There are several branches for use when restripping different data sets.

# Making changes:

Before creating a merge request, please consider the following guidelines:

- the master branch is intended to be used for Upgrade specific MRs,
- bug fixes for 2018 data-taking should be committed as MRs to 2018-patches,
- any other changes for Run1 and Run2 analysis should go to run2-patches.

We will propagate those changes to master if relevant for the upgrade or run2-patches if appropriate.

Please contact the Stripping coordinators if you are unsure which target branch to choose. 

For new campaigns, we will inform you of the dedicated branches.

# Accessing data in the Continuous Integration (CI) jobs

Due to the authentication requirement necessary for accessing data in the CI jobs, one must allow the repository to authenticate you.  This is performed using the ```apd``` package, and is similar to the procedure of the Run-3 workflow for AnalysisProductions.  To perform this authentication step, one should follow the steps below:
- (1) Go to the APD page for the repository (https://lhcb-analysis-productions.web.cern.ch/settings/?project_id=543&project_path=lhcb/Stripping)
- (2) Add the path to the data to the "Accessible locations" path (the path to add is ```/eos/lhcb/cern-swtest/```)
- (3) Click "Register this repository" and ensure that the project name and accessible locations is sensible

# 2023 Run 2 Incremental Restripping Campaign

The 2023 incremental restripping campaign workflow has been simplified for production:

- Analysers should branch from their PAWGs ```PAWG_2018-patches``` development branch, and do developments there.
- When ready, these developments are merged to the PAWGs development branch, overseen by liaisons.
- Incremental merges of the development branches to the production branch of ```2018-patches``` will be done by the stripping coordinators.

Further details can be found in the training item https://indico.cern.ch/event/1274062/#b-513813-stripping-liaisons-tr
