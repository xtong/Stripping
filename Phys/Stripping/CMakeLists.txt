###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/Stripping
--------------
#]=======================================================================]

gaudi_install(PYTHON)
gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    DAQ/DAQSys
    GaudiAlg:GaudiAlg
    GaudiAud:GaudiAud
    GaudiCommonSvc:GaudiCommonSvc
    GaudiCoreSvc:GaudiCoreSvc
    Kernel/LHCbKernel
    Phys/DaVinciTools:DaVinciTools
    Phys/DSTWriters
    Phys/JetAccessories:JetAccessories
    Phys/StrippingAlgs:StrippingAlgs
    Tr/TrackExtrapolators:TrackExtrapolators
)

gaudi_add_tests(QMTest)
