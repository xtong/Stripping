/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FilterVtxTopoTracksTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FilterVtxTopoTracksTool
//
// 2012-10-24 : Julien Cogan and Mathieu Perrin-Terrin
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( FilterVtxTopoTracksTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FilterVtxTopoTracksTool::FilterVtxTopoTracksTool( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : GaudiTool( type, name, parent ), m_tracks( 0 ) {
  declareInterface<IFilterVtxTopoTracksTool>( this );

  declareProperty( "InputTrackLocation", m_inputTrackLocation = "/Event/Rec/Track/Best" );
}

//=============================================================================
// Destructor
//=============================================================================
FilterVtxTopoTracksTool::~FilterVtxTopoTracksTool() {}

//=============================================================================
std::vector<const LHCb::Track*>& FilterVtxTopoTracksTool::filteredTracks( Tuples::Tuple* /* tuple */ ) {
  m_tracks.clear();
  // tuple = 0;
  // if (tuple) { }

  LHCb::Tracks* trackContainer = get<LHCb::Tracks*>( m_inputTrackLocation );
  m_tracks.reserve( trackContainer->size() );
  for ( LHCb::Tracks::iterator it_ptrk = trackContainer->begin(); it_ptrk < trackContainer->end(); ++it_ptrk ) {
    m_tracks.push_back( *it_ptrk );
  }

  return m_tracks;
}
