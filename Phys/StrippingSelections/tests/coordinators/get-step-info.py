#!/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys, subprocess
from DIRAC.Core.Base.Script import parseCommandLine
parseCommandLine() # Need this for some reason.
from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequest import ProductionRequest

def step_info(stepid) :
    pr = ProductionRequest()
    pr.stepsList = [stepid]
    pr.resolveSteps()
    return pr.stepsListDict[0]

if __name__ == '__main__' :
    stepinfo = step_info(int(sys.argv[1]))
    print 'dvversion=' + stepinfo['ApplicationVersion']
    print 'conddb=' + stepinfo['CONDDB']
    print 'dddb=' + stepinfo['DDDB']
    opts = filter(lambda opt : 'Stripping' in opt, stepinfo['OptionFiles'].split(';'))[0]
    print 'opts=' + opts.strip().replace('$', '\\$')
    platform = stepinfo['SystemConfig']
    if platform == 'NULL' or not platform :
        proc = subprocess.Popen(('lb-sdb-query listPlatforms DaVinci ' + stepinfo['ApplicationVersion']).split(),
                                stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        stdout, stderr = proc.communicate()
        platform = filter(lambda plat : all(test in plat for test in ('x86_64', 'slc', 'gcc', 'opt')), 
                          stdout.split('\n'))
        if platform :
            platform = platform[0]
        else :
            platform = ''
    print 'platform=' + platform
