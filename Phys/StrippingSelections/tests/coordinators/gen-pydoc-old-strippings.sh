#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Generate python doc for older stripping versions.

stripping=$1
datatype=$2
stepid=$3
cmt=$4
if [ -z "$cmt" ] ; then
    if [ ! -e $LHCb_release_area/DAVINCI/DAVINCI_${dvversion}/CMakeLists.txt ] ; then
	cmt=1
    else
	cmt=0
    fi
fi


eval "$(lb-run -c x86_64-slc6-gcc62-opt LHCbDirac/v8r7p1 ./get-step-info.py $stepid)"

echo $stripping
echo $dvversion
echo $datatype
echo $stepid
echo

stripver=$(lb-sdb-query listDependencies DaVinci $dvversion | grep STRIPPING | awk '{print $2;}')
physver=$(lb-sdb-query listDependencies DaVinci $dvversion | grep PHYS | awk '{print $2;}')

if [ -e $stripping ] ; then
    rm -rf $stripping
fi
mkdir $stripping
cd $stripping
. LbLogin.sh -c ${platform}

if [ 0 = $cmt ] ; then
    lb-dev DaVinci/${dvversion}
    cd DaVinciDev_${dvversion}
else 
    User_release_area=$PWD
    CMTPROJECTPATH="$PWD:$CMTPROJECTPATH"
    . SetupProject.sh --build-env --user-area $PWD DaVinci $dvversion
    git init
fi

git lb-use Stripping
for pkg in StrippingConf StrippingUtils ; do
    git lb-checkout Stripping/$stripver Phys/${pkg}
done
for fname in StrippingConf/python/StrippingConf/StrippingLine.py StrippingConf/python/StrippingConf/Utils.py StrippingConf/python/StrippingConf/StrippingStream.py StrippingUtils/python/StrippingUtils/Utils.py ; do
    git lb-checkout Stripping/S29-add-strippingdoc-pkg Phys/$fname
done

git lb-use Phys
git lb-checkout Phys/$physver Phys/SelPy
git lb-checkout Phys/2017-patches Phys/SelPy/python/SelPy/utils.py

fname=$(python -c "print '${stripping}.py'.lower()")

if [ 0 = $cmt ] ; then
    make configure && make

# For S22b.
    stripping=${stripping/b/}

    opts=$(./run echo $opts)
    tck=$(grep -o '0x[0-9]\+' $opts)
    if [ -z "$tck" ] ; then
	tck=0
    #else
    #tck=$(python -c "print $tck")
    fi

    desc=$(./run python -c "from StrippingArchive import _stripping_help; print _stripping_help['$stripping']")
    echo "DaVinciVersion = '$dvversion'
datatype = '$datatype'
platform = '$platform'
stepid = $stepid
CondDBtag = '$conddb'
DDDBtag = '$dddb'
TCK = $tck
description = '$desc'
" > $fname
    
    ./run python -c "execfile('$opts')
from StrippingUtils.Utils import LineBuilder
from Configurables import DaVinci
DaVinci().DataType = '$datatype'

from GaudiPython.Bindings import AppMgr
AppMgr().initialize()

linedoc = {}
if 'stream' in globals() :
    streams = [stream]
for stream in streams :
            streamname = stream.name() + ('.mdst' if stream.name() in {'ALLMDST', 'Bhadron', 'Charm', 'Leptonic', 'PID'} else '.dst')
            for line in stream.lines :
                if line.name() in linedoc :
                    linedoc[line.name()]['streams'].append(streamname)
                    continue
                thislinedoc = line.python_doc()
                thislinedoc['streams'] = [streamname]
                builder = LineBuilder.find_builder_from_line(line)
                thislinedoc['buildername'] = builder._name
                thislinedoc['buildertype'] = builder.__class__.__name__
                thislinedoc['buildermodule'] = builder.__module__
                mod = __import__(builder.__module__, fromlist = ['__author__'])
                try :
                    thislinedoc['author'] = mod.__author__
                except AttributeError :
                    thislinedoc['author'] = None
                linedoc[line.name()] = thislinedoc
from pprint import pformat
with open('$fname', 'a') as fdoc :
    fdoc.write('lines = ' + pformat(linedoc).replace('\n', '\n' + (' ' * len('lines = '))))
"
else
    for pkg in StrippingConf StrippingUtils SelPy ; do
	cd Phys/$pkg/cmt
	cmt config
	cmt make
	. ./setup.sh
	cd ../../../
    done
    # Need to do this to get the full environment, not just the build env.
    . SetupProject.sh --user-area $PWD/.. DaVinci $dvversion

    opts=$(eval "ls ${opts/\\/}")

    tck=$(grep -o '0x[0-9]\+' $opts)
    if [ -z "$tck" ] ; then
	tck=0
    fi

    desc=$(python -c "from StrippingArchive import _stripping_help; print _stripping_help['$stripping']" | tail -n 1)

    echo "DaVinciVersion = '$dvversion'
datatype = '$datatype'
platform = '$platform'
stepid = $stepid
CondDBtag = '$conddb'
DDDBtag = '$dddb'
TCK = $tck
description = '$desc'
" > $fname
    
    python -c "
# We only need the stream definitions, if it crashes elsewhere we don't care.
try :
  execfile('$opts')
except :
  pass

from StrippingUtils.Utils import LineBuilder
from Configurables import DaVinci
DaVinci().DataType = '$datatype'

if 'stream' in globals() :
    streams = [stream]

from GaudiPython.Bindings import AppMgr
AppMgr().initialize()

linedoc = {}
for stream in streams :
            streamname = stream.name() + ('.mdst' if stream.name() in {'ALLMDST', 'Bhadron', 'Charm', 'Leptonic', 'PID'} else '.dst')
            for line in stream.lines :
                if line.name() in linedoc :
                    linedoc[line.name()]['streams'].append(streamname)
                    continue
                thislinedoc = line.python_doc()
                thislinedoc['streams'] = [streamname]
                builder = LineBuilder.find_builder_from_line(line)
                thislinedoc['buildername'] = builder._name
                thislinedoc['buildertype'] = builder.__class__.__name__
                thislinedoc['buildermodule'] = builder.__module__
                mod = __import__(builder.__module__, fromlist = ['__author__'])
                try :
                    thislinedoc['author'] = mod.__author__
                except AttributeError :
                    thislinedoc['author'] = None
                linedoc[line.name()] = thislinedoc
from pprint import pformat
with open('$fname', 'a') as fdoc :
    fdoc.write('lines = ' + pformat(linedoc).replace('\n', '\n' + (' ' * len('lines = '))))
"
fi