#!/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os, sys
sys.path.insert(0, os.path.join(os.environ['STRIPPINGSELECTIONSROOT'], 'tests', 'python'))
from StrippingTests.WGLines import TableMaker

def main() :
    '''Take two files containing the breakdown of stripping lines per WG and stream
    for two stripping versions, made by get_wg_lines.py, and compare their number of 
    lines, their intersection, and their difference (by line names).'''

    from argparse import ArgumentParser

    argparser = ArgumentParser()
    argparser.add_argument('file1')
    argparser.add_argument('file2')
    
    args = argparser.parse_args()
    with open(args.file1) as f1 :
        lines1 = TableMaker(eval(f1.read()))
    with open(args.file2) as f2 :
        lines2 = TableMaker(eval(f2.read()))

    allwgs = set(lines1.keys()).union(lines2.keys())
    lines1.update_columns(allwgs)
    lines2.update_columns(allwgs)
    
    allstreams = set(lines1.values()[0].keys()).union(lines2.values()[0].keys())
    lines1.update_rows(allstreams)
    lines2.update_rows(allstreams)

    f1name = args.file1[:-3]
    f2name = args.file2[:-3]
    lines1.counts_table(f1name + '_counts')
    lines2.counts_table(f2name + '_counts')

    # Lines in both Stripping versions.
    lines2.intersection_counts_table(lines1, f2name + '_intersection_' + f1name)
    # Lines in Stripping1 and not in Stripping2
    lines1.diff_counts_table(lines2, f1name + '_diff_' + f2name)
    # Lines in Stripping2 and not in Stripping1
    lines2.diff_counts_table(lines1, f2name + '_diff_' + f1name)

    texname = f1name + '_comparison_' + f2name + '.tex'
    with open(texname, 'w') as ftex :
        ftex.write(r'''
\documentclass{beamer}
\mode<presentation>

\begin{document}

\begin{frame}
\frametitle{%s counts}
\begin{table}
  \resizebox{\textwidth}{!}{
    \input{%s_counts.tex}
  }
\end{table}
\end{frame}

\begin{frame}
\frametitle{%s counts}
\begin{table}
  \resizebox{\textwidth}{!}{
    \input{%s_counts.tex}
  }
\end{table}
\end{frame}

\begin{frame}
\frametitle{%s \& %s intersection}
\begin{table}
  \resizebox{\textwidth}{!}{
    \input{%s_intersection_%s.tex}
  }
\end{table}
\end{frame}

\begin{frame}
\frametitle{%s \& %s difference}
\begin{table}
  \resizebox{\textwidth}{!}{
    \input{%s_diff_%s.tex}
  }
\end{table}
\end{frame}

\begin{frame}
\frametitle{%s \& %s difference}
\begin{table}
  \resizebox{\textwidth}{!}{
    \input{%s_diff_%s.tex}
  }
\end{table}
\end{frame}

\end{document}''' % (f1name, f1name, f2name, f2name, f2name, f1name, f2name, f1name, f1name, f2name, f1name, f2name, f2name, f1name, f2name, f1name))

    return lines1, lines2

if __name__ == '__main__' :
    lines1, lines2 = main()
