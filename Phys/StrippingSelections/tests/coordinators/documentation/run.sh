#!/bin/sh
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#When running you will recieve errors regarding Hlt1DecReportsDecoder, this does not affect the production of the documentation


gaudirun.py $APPCONFIGROOT/options/DaVinci/DV-Stripping28-Stripping.py $APPCONFIGOPTS/DaVinci/DataType-2016.py $APPCONFIGOPTS/DaVinci/InputType-RDST.py $APPCONFIGOPTS/DaVinci/DV-RawEventJuggler-0_3-to-4_2.py $STRIPPINGSELECTIONSROOT/tests/data/Reco16_Run182594.py DaVinci-Nevents.py tck.py | tee log
