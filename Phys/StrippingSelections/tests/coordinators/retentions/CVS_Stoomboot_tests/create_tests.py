###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

for WG in ['B2CC', 'BandQ', 'BnoC', 'B2OC', 'Calib', 'Charm', 'QEE', 'Semileptonic', 'RD']:
  os.system('cp TestFromSettings_all.py TestFromSettings_'+WG+'.py')
  os.system("sed -i 's/___my_wg___/"+WG+"/g' TestFromSettings_"+WG+".py")
