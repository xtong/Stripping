#!/user/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from ROOT import *
import collections

f=open("retentions/line_retentions.txt","r")
lines = f.readlines()
d={}
for line in lines:
  el = line.split('|')
  try:
    if 'Stripping' in el[1]:
      try:
        d[el[3]].append(el[1])
      except:
        d[el[3]] = []
        d[el[3]].append(el[1])
  except:
    pass
od = collections.OrderedDict(sorted(d.items(), reverse=True))
c=TCanvas()
h=TGraph()
i=0
for key in od.keys():
  if len(od[key])==1:
    h.SetPoint(i,i,float(key))
    i+=1
  else:
    for line in od[key]:
      h.SetPoint(i,i,float(key))    
      i+=1
h.GetYaxis().SetTitle("Retention (%)")
c.SetLogy()
h.GetXaxis().SetTitle("Line number")
h.Draw("AC")
c.SaveAs("Retentions.pdf")

TotalRetention = 0
ParetoRetention = 0
counted = 0
for key in od.keys():
  TotalRetention += float(key)*len(od[key])
  counted += len(od[key])
  if counted < i*0.05:
    ParetoRetention += float(key)*len(od[key])
print TotalRetention, ParetoRetention, ParetoRetention/TotalRetention

