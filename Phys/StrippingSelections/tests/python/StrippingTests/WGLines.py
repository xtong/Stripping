###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Get the breakdown of lines by WG and stream for a given stripping version.'''

from Utils import *
from pprint import pformat

def set_single_wg(name, builderconf) :
    if not 'CONFIG' in builderconf :
        for subname, subconf in builderconf.iteritems() :
            set_single_wg(subname, subconf)
        return

    if len(builderconf['WGs']) > 1 :
        print 'WARNING: builder {0!r} belongs to more than one WG: {1!r}. Assigning it only to {2!r}'\
            .format(name, builderconf['WGs'], builderconf['WGs'][0])
        builderconf['WGs'] = [builderconf['WGs'][0]]
        
def get_wg_lines(strippingconfversion, strippingarchiveversion,
                 wgs = ('B2CC', 'B2OC', 'BandQ', 'Charm', 'BnoC', 'QEE', 'RD', 'Semileptonic', 'ALL')) :
    '''Get the breakdown of lines by WG and stream for a given stripping version.
    strippingconfversion is the version of the config from StrippingSettings to use.
    If it's None then the default dicts from StrippingSelections are used.
    strippingarchiveversion is the version of the lines to use from StrippingArchive.
    If it's None then the default lines from StrippingSelections are used.'''

    strippingconfversion = stripping_name(strippingconfversion)
    strippingarchiveversion = stripping_name(strippingarchiveversion)

    conf = stripping_config(strippingconfversion)
    conf = dict(conf)
    set_single_wg(strippingconfversion, conf)
    
    if strippingarchiveversion :
        archive = StrippingArchive.strippingArchive(strippingarchiveversion)
    else :
        archive = None

    lines = {}
    for wg in wgs :
        wglines = {}
        lines[wg] = wglines
        streams = build_streams(conf, archive = archive, WGs = [wg])
        for stream in streams :
            streamlines = tuple(line.name() for line in stream.lines if line._appended and line.prescale != 0.)
            wglines[stream.name()] = streamlines
    return lines
    

class TableMaker(dict) :
    '''Helper class to make tables of numbers of lines.'''

    def __init__(self, lines, default = tuple) :
        dict.__init__(self, lines)
        allstreams = set()
        for wglines in lines.values() :
            allstreams.update(set(wglines.keys()))
        self.update_rows(allstreams)

    def update_rows(self, rownames, default = tuple) :
        for wg, wglines in self.iteritems() :
            for stream in rownames :
                if not stream in wglines :
                    wglines[stream] = default()

    def update_columns(self, columnnames, default = dict) :
        for name in columnnames :
            if not name in self :
                self[name] = default()

    def count_lines(self) :
        counts = TableMaker({})
        total = 0
        streamtots = dict((stream, 0) for stream in self.values()[0].keys())
        for wg, wglines in self.iteritems() :
            counts[wg] = {}
            wgtot = 0
            for stream, streamlines in wglines.iteritems() :
                counts[wg][stream] = len(streamlines)
                wgtot += counts[wg][stream]
                streamtots[stream] += counts[wg][stream]
            counts[wg]['Total'] = wgtot
            total += wgtot
        counts['Total'] = streamtots
        counts['Total']['Total'] = total
        return counts 

    def operation(self, lines2, func, default = tuple) :
        resultlines = TableMaker({})
        for wg, wglines in self.iteritems() :
            resultlines[wg] = {}
            if not wg in lines2 :
                continue
            for stream, streamlines in wglines.iteritems() :
                resultlines[wg][stream] = default()
                if not stream in lines2[wg] :
                    continue
                resultlines[wg][stream] = func(streamlines, lines2[wg][stream])
        return resultlines

    def intersection(self, lines2) :
        return self.operation(lines2, (lambda x, y : tuple(set(x).intersection(set(y)))))

    def difference(self, lines2) :
        return self.operation(lines2, (lambda x, y : tuple(set(x).difference(set(y)))))

    def zip(self, lines2) :
        return self.operation(lines2, (lambda x, y : (x, y)))

    def table(self, format = str) :
        table = '\\begin{tabular}{c|' + 'c' * len(self) + '}\n'
        for wg in sorted(self.keys()) :
            table += ' & \\rotatebox{90}{' + wg + '} '
        table += r'\\' + '\n\\hline '
        for stream in sorted(self.values()[0].keys()) :
            table += stream
            for wg in sorted(self.keys()) :
                table += ' & ' + format(self[wg][stream])
            table += r'\\' + '\n'
        table += '\\end{tabular}\n'
        return table

    def counts_table(self, outputfname) :
        table = self.count_lines().table()
        if outputfname :
            if outputfname[-4:] != '.tex' :
                outputfname += '.tex'
            with open(outputfname, 'w') as f :
                f.write(table)
        return table

    def operation_counts_table(self, lines2, operation, outputfname = None) :
        result = operation(self, lines2)
        normcounts = result.count_lines().zip(lines2.count_lines())
        countstable =  normcounts.table(lambda val : '{0}/{1} ({2})'.format(val[0], val[1], 
                                                                            ('{:.2f}'.format(float(val[0])/val[1]) if val[1] != 0 else '-')))
        if outputfname :
            with open(outputfname + '.py', 'w') as f :
                f.write(pformat(result))
            with open(outputfname + '.tex', 'w') as f :
                f.write(countstable)
        return result, normcounts, countstable

    def intersection_counts_table(self, lines2, outputfname = None) :
        return self.operation_counts_table(lines2, TableMaker.intersection, outputfname)

    def diff_counts_table(self, lines2, outputfname = None) :
        return self.operation_counts_table(lines2, TableMaker.difference, outputfname)

