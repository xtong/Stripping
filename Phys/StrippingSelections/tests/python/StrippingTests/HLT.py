###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Test HLT regexs.'''

import os, subprocess

def get_hlt_lines(tck, outputfname = None) :
    if os.path.exists(tck + '-Lines.py') :
        return get_hlt_lines(tck + '-Lines.py')
    if os.path.exists(tck) :
        with open(tck) as f :
            lines = eval(f.read())
        return lines
    if not outputfname :
        outputfname = tck + '-Lines.py'
    args = ['lb-run', 'Moore/latest', 'python', 
            os.path.join(os.environ['STRIPPINGSELECTIONSROOT'], 
                         'tests', 'coordinators', 'get_hlt_lines.py'),
            '--tck', tck, '--outputfile', outputfname]
    callval = subprocess.call(args)
    if 0 != callval :
        raise ValueError('Failed to call\n' + ' '.join(args) + '\nReturned' + str(callval))
    return get_hlt_lines(outputfname)
    
def check_hlt_filters(streams, hlt1tck, hlt2tck = None) :
    hlt1lines = get_hlt_lines(hlt1tck)
    if not hlt2tck :
        hlt2lines = hlt1lines['Hlt2']
    else :
        hlt2lines = get_hlt_lines(hlt2tck)['Hlt2']
    hlt1lines = hlt1lines['Hlt1']
    nomatches = {}
    for stream in streams :
        for line in stream.lines :
            matches = line.check_hlt_filters(hlt1lines, hlt2lines)
            if not all(matches.values()) :
                streamnomatches = nomatches.get(stream.name(), {})
                streamnomatches[line.name()] = tuple(expr for expr, ms in matches.iteritems() if not ms)
                nomatches[stream.name()] = streamnomatches
    return nomatches
