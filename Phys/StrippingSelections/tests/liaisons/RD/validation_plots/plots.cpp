/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Guido Andreassi <guido.andreassi@cern.ch>
// This macro produces the mass plots for the validation of stripping data in Rare Decays.

int plot( TFile* f, std::string tupname, std::string var, std::string cut );

void plots() {

  TFile* file_leptonic = TFile::Open( "S29_validation_leptonic.root" );

  // plot(file_leptonic, "Ups2ee", "Upsilon_1S_M", "eplus_ProbNNe>0.7 && eminus_ProbNNe>0.7 &&
  // Upsilon_1S_ENDVERTEX_CHI2<2");
  plot( file_leptonic, "Lb2JpsieeL", "J_psi_1S_M", "J_psi_1S_M>1500 && J_psi_1S_M<3500" );
  plot( file_leptonic, "Lb2JpsimmL", "J_psi_1S_M", "J_psi_1S_M>2500 && J_psi_1S_M<3500" );
  plot( file_leptonic, "JpsieeK", "Bplus_M", "" );
  plot( file_leptonic, "JpsimmK", "Bplus_M", "" );
  plot( file_leptonic, "JpsieeK", "J_psi_1S_M", "" );
  plot( file_leptonic, "JpsimmK", "J_psi_1S_M", "" );

  TFile* file_bhadron = TFile::Open( "S29_validation_bhadroncomplete.root" );

  plot( file_bhadron, "B2KstGamma", "B0_M",
        "TMath::Max(Kplus_PT,piminus_PT) > 1700 && TMath::Min(Kplus_PT,piminus_PT) > 500 && Kplus_ProbNNpi < 0.2 && "
        "Kplus_ProbNNk > 0.2 && piminus_ProbNNpi > 0.2" );
  plot( file_bhadron, "B2KstGamma", "Kst_892_0_M",
        "TMath::Max(Kplus_PT,piminus_PT) > 1700 && TMath::Min(Kplus_PT,piminus_PT) > 500 && Kplus_ProbNNpi < 0.2 && "
        "Kplus_ProbNNk > 0.2 && piminus_ProbNNpi > 0.2" );

  exit( 0 );
}

int plot( TFile* f, std::string tupname, std::string var, std::string cut ) {

  TTree*   tree = (TTree*)f->Get( ( tupname + "/DecayTree" ).c_str() );
  TCanvas* c    = new TCanvas( "c", "c" );
  c->cd();
  TH1D* h = new TH1D( "h", ( var + " from " + tupname ).c_str(), 100, 0, 0 );
  tree->Draw( ( var + ">>h" ).c_str(), cut.c_str() );
  c->SaveAs( ( var + "_" + tupname + ".pdf" ).c_str() );
  delete h;
  delete c;
  return ( 0 );
}
