#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os

# app = DaVinci( version = 'v39r0p1' ) # S26, no longer compat with Ganga 6.0.44
app = DaVinci( version = 'v38r1p1' ) # need local redirection
app.user_release_area = os.path.expandvars('$HOME/cmtuser/')
# app.setupProjectOptions = '--no-user-area'
app.optsfile  = 'TestS26_Selections.py'
app.extraopts = """
from Configurables import DaVinci
DaVinci().EvtMax = -1
"""

## S24
# ds = LHCbDataset.new('../../data/Reco15a_Run164668.py', catalog=False)[0:100]
# ds.XMLCatalogueSlice = '../../data/Reco15a_Run164668.xml'

## S21rXp1
# ds = app.readInputData('TestS21rXp1_settings.py')
# ds.XMLCatalogueSlice = '../../data/pool_xml_catalog_Reco14_Run125113.xml'

## S26
ds = app.readInputData('../../data/Reco15a_Run164668.py')
ds.XMLCatalogueSlice = '../../data/Reco15a_Run164668.xml'

j = Job()
j.name        = 'TestS26'
j.comment     = 'Deletable: Test full QEE'
j.backend     = PBS(extraopts='--mem=3600 -t 1-0:0:0')
# j.backend     = Dirac()
j.application = app
j.inputdata   = ds
j.splitter    = SplitByFiles( filesPerJob=1 )
j.outputfiles = [ 'summary.xml' ] #, LocalFile('*.dst') ]

queues.add(j.submit)
