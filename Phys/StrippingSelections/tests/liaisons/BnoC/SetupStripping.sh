#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

lb-dev DaVinci/v42r3  #Version of DaVinci will probably change for each campaign, usually announced or detailed on stripping twiki.
cd DaVinciDev_v42r3
git lb-use Stripping
git lb-checkout Stripping/master Phys/StrippingSelections
git lb-checkout Stripping/master Phys/StrippingSettings
getpack TMVAWeights HEAD  #Only required if there are lines with TMVA Weights files not yet in a released version of TMVAWeights
make configure
make
make install