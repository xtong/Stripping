###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id: $
# Test your line(s) of the stripping
#
# NOTE: Please make a copy of this file for your testing, and do NOT change this one!
#

from Gaudi.Configuration import *
from Configurables import DaVinci
from StrippingConf.Configuration import StrippingConf

# Specify the name of your configuration
datatype = '2016'  #FOR USERS
confname = 'Beauty2Charm'  #FOR USERS

# NOTE: this will work only if you inserted correctly the
# default_config dictionary in the code where your LineBuilder
# is defined.
from StrippingSelections import buildersConf
confs = buildersConf()
from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
#confs[confname]["CONFIG"]["SigmaPPi0CalPrescale"] = 0.5 ## FOR USERS, YOU ONLY NEED TO QUICKLY MODIFY CutName and NewValue (no need to recompile the package but please update the default_config before committing)
streams = buildStreamsFromBuilder(confs, confname)

#clone lines for CommonParticles overhead-free timing
print "Creating line clones for timing"
for s in streams:
    for l in s.lines:
        if "_TIMING" not in l.name():
            cloned = l.clone(l.name().strip("Stripping") + "_TIMING")
            s.appendLines([cloned])

#define stream names
leptonicMicroDSTname = 'Leptonic'
charmMicroDSTname = 'Charm'
pidMicroDSTname = 'PID'
bhadronMicroDSTname = 'Bhadron'
mdstStreams = [
    leptonicMicroDSTname, charmMicroDSTname, pidMicroDSTname,
    bhadronMicroDSTname
]
dstStreams = [
    "BhadronCompleteEvent", "CharmCompleteEvent", "Dimuon", "EW",
    "Semileptonic", "Calibration", "MiniBias", "Radiative"
]

stripTESPrefix = 'Strip'

from Configurables import ProcStatusCheck

sc = StrippingConf(
    Streams=streams,
    MaxCandidates=2000,
    AcceptBadEvents=False,
    BadEventSelection=ProcStatusCheck(),
    TESPrefix=stripTESPrefix,
    ActiveMDSTStream=True,
    Verbose=True,
    DSTStreams=dstStreams,
    MicroDSTStreams=mdstStreams)

#
# Configure the dst writers for the output
#
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (
    SelDSTWriter, stripDSTStreamConf, stripDSTElements,
    stripMicroDSTStreamConf, stripMicroDSTElements,
    stripCalibMicroDSTStreamConf)

#
# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
mdstStreamConf = stripMicroDSTStreamConf(
    pack=enablePacking, selectiveRawEvent=True)
mdstElements = stripMicroDSTElements(pack=enablePacking)

#
# Configuration of SelDSTWriter
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#
SelDSTWriterElements = {
    'default': stripDSTElements(pack=enablePacking),
    charmMicroDSTname: mdstElements,
    leptonicMicroDSTname: mdstElements,
    pidMicroDSTname: mdstElements,
    bhadronMicroDSTname: mdstElements
}

SelDSTWriterConf = {
    'default':
    stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=True),
    charmMicroDSTname:
    mdstStreamConf,
    leptonicMicroDSTname:
    mdstStreamConf,
    bhadronMicroDSTname:
    mdstStreamConf,
    pidMicroDSTname:
    stripCalibMicroDSTStreamConf(pack=enablePacking, selectiveRawEvent=True)
}

dstWriter = SelDSTWriter(
    "MyDSTWriter",
    StreamConf=SelDSTWriterConf,
    MicroDSTElements=SelDSTWriterElements,
    OutputFileSuffix='000000',
    SelectionSequences=sc.activeStreams())

#
# Add stripping TCK
#
#from Configurables import StrippingTCK
#stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36112100)

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60

from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append(ChronoAuditor("Chrono"))

from Configurables import StrippingReport
# sr = StrippingReport(Selections = sc.selections())
sr = StrippingReport(Selections=sc.selections(), ReportFrequency=5000)

from Configurables import AlgorithmCorrelationsAlg
ac = AlgorithmCorrelationsAlg(Algorithms=list(set(sc.selections())))

if datatype == '2016' or datatype == '2015':
    importOptions(
        "$STRIPPINGSELECTIONSROOT/tests/coordinators/DV-RedoCaloPID-Stripping_28_24.py"
    )
    # Items that might get lost when running the CALO+PROTO ReProcessing in DV
    caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
    # Make sure they are present on full DST streams
    SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

#Configure DaVinci
DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().EvtMax = 25000
DaVinci().PrintFreq = 1000
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([sr])
# DaVinci().appendToMainSequence( [ ac ] )
# DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# for better performance - run on all Members and use same options as in AppConfig
from Configurables import EventTuple, TupleToolEventInfo
evt = EventTuple('TIMER')
evt.ToolList = []  # empty the tool list by default
evt.addTool(TupleToolEventInfo, name="TupleToolEventInfo")
strippingAlgorithms = DaVinci().mainSeq.Members[0].allConfigurables.keys(
)  # full Members list includes also the report
evt.TupleToolEventInfo.Algorithms += strippingAlgorithms
evt.ToolList = ["TupleToolEventInfo"]  # add it to the list
DaVinci().appendToMainSequence([evt])
DaVinci().TupleFile = 'TIMER.root'

# change the column size of timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

from Configurables import RootCnvSvc
RootCnvSvc().GlobalCompression = "ZLIB:1"

# Data
if datatype == '2016':
    importOptions(
        "$STRIPPINGSELECTIONSROOT/tests/data/Reco16_DataType2016_Run174858.py")
    importOptions(
        "$STRIPPINGSELECTIONSROOT/tests/data/Reco16_DataType2016_Run174858_DV.py"
    )
elif datatype == '2015':
    importOptions(
        "$STRIPPINGSELECTIONSROOT/tests/data/Reco15a_DataType2015_Run164524.py"
    )
    importOptions(
        "$STRIPPINGSELECTIONSROOT/tests/data/Reco15a_DataType2015_Run164524_DV.py"
    )
else:
    importOptions(
        "$STRIPPINGSELECTIONSROOT/tests/data/Reco18_DataType2018_Run214741.py")
    importOptions(
        "$STRIPPINGSELECTIONSROOT/tests/data/Reco18_DataType2018_Run214741_DV.py"
    )
