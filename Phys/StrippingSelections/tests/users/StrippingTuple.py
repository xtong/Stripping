###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''Functionality for making ntuples from the output of stripping lines.'''

from StrippingDoc import StrippingDoc
from DecayTreeTuple.Configuration import DecayTreeTuple
from Configurables import DaVinci, LoKi__HDRFilter, LoKi__Hybrid__TupleTool, GaudiSequencer

def add_stripping_tuple_sequence(version, linename, fulldescriptors = True, stream = None) :
    '''Add a sequence to make an ntuple, from a specific stripping line given 
    the stripping version, to DaVinci().UserAlgorithms. The tuple should include
    any RelatedInfos.
    
    If fulldescriptors is True, the tuple will use the expanded 
    DecayDescriptors including all daughters (not guaranteed to work for all 
    lines) else it'll just fill the particles from the first level of the 
    decay.
    
    You should only need to specify the stream if the line is run in more 
    than one stream, or if you're running on MC. In that case, the stream 
    should include the suffix (.dst or .mdst).'''

    strippingdoc = StrippingDoc(version)
    linedoc = strippingdoc.get_line(linename)
    seq = stripping_tuple_sequence_from_doc(linedoc, fulldescriptors, stream)
    DaVinci().UserAlgorithms += [seq]
    DaVinci().TupleFile = 'DaVinciTuples.root'
    # Sets DataType, InputType, and RootInTES
    DaVinci(**linedoc.davinci_config(stream))
    # Return the DecayTreeTuple.
    return seq.Members[1]

def stripping_tuple_sequence_from_doc(linedoc, fulldescriptors = True, stream = None) :
    '''Make a GaudiSequencer to make an ntuple from the output of a 
    stripping line, given the StrippingLineDoc instance. It first 
    checks for a positive decistion then fills the ntuple. It should
    include any RelatedInfos. 

    If fulldescriptors is True, the tuple will use the expanded 
    DecayDescriptors including all daughters (not guaranteed to work for all 
    lines) else it'll just fill the particles from the first level of the 
    decay.

    You should only need to specify the stream if the line is run in 
    more than one stream, or if you're running on MC. In that case, 
    the stream should include the suffix (.dst or .mdst).'''

    seq = GaudiSequencer(linedoc.name + 'Sequence')
    linefilter = LoKi__HDRFilter(linedoc.name + 'Filter',
                                 **linedoc.decision_filter_config())
    dtt = DecayTreeTuple(linedoc.name + 'Tuple',
                         UseLabXSyntax = True,
                         RevertToPositiveID = False,
                         **linedoc.tuple_config(fulldescriptors))

    # Add tool to retrieve RelatedInfos if necessary.
    if linedoc.RelatedInfoTools :
        relinfoconfig = linedoc.related_info_config(stream)
        relinfotool = LoKi__Hybrid__TupleTool(linedoc.name + 'RelInfoTuple',
                                              **relinfoconfig)
        # Add a branch for the head of the decay, so the related infos are only filled for it.
        dtt.addBranches({'lab0' : dtt.Decay.replace('^', '')})
        dtt.lab0.addTupleTool(relinfotool)
    seq.RootInTES = linedoc.root_in_TES(stream)
    seq.Members = [linefilter, dtt]
    return seq
