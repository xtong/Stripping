#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2020 for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
import sys

from StrippingConf.Configuration import StrippingConf
from StrippingSelections import buildersConf
from StrippingSelections.Utils import buildStreams

confs = buildersConf()
allStreams = buildStreams(confs)


def test_stream_locations():
    streamLocations = {}
    for stream in allStreams:
        streamLocations[stream.name()] = [
            stream.name() + '/' + loc for loc in stream.outputLocations()
        ]
    return streamLocations


def check_location_validity(loc):
    splitLocation = loc.replace('/Event/', '').split('/')
    return splitLocation[0] == 'Phys' and len(splitLocation) > 1


def test_locations_are_valid():
    '''
    Check that the stream outputLocations start with "/Event/Phys/" of "Phys/"
    '''
    summary = {}
    for stream in allStreams:
        for loc in stream.outputLocations():
            valid = check_location_validity(loc)
            summary[stream.name()] = valid
            if not valid:
                print('streamName has invalid OutputLocation', loc)
    assert (not False in summary)


def test_locations_are_unique():
    lineLocations = {}
    for stream in allStreams:
        for line in stream.lines:
            if line.outputLocation():
                lineLocations[line.name()] = line.outputLocation()
    locations = list(lineLocations.values())
    badLines = []
    for loc in locations:
        if locations.count(loc) != 1:
            badLines.append(loc)
    if len(badLines) > 0:
        message = 'Found locations written out by more than one line:\n' + str(
            badLines) + '\n'
        sys.stderr.write(message)
        assert (False)
