#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from py.test import raises


def test_checkConfig_raises_KeyError_for_missing_parameter():
    from StrippingUtils.Utils import checkConfig

    ref_params = ('A', 'B', 'C', 'D')

    config = {'A': 1, 'B': 2, 'C': 3}

    raises(Exception, checkConfig, ref_params, config)


def test_checkConfig_raises_KeyError_for_too_many_parameters():
    from StrippingUtils.Utils import checkConfig

    ref_params = ('A', 'B', 'C', 'D')

    config = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5}

    raises(Exception, checkConfig, ref_params, config)
