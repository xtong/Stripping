###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
#   Stripping selections 
#
#   @author A. Poluektov
#   @date 2009-05-15
#
from Gaudi.Configuration import *

#
# Import stripping selections
#

# Angelo's selection
importOptions( "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingB2Charged2Body.py")


importOptions( "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingB2DPi.py")

# Rob's selection
importOptions( "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingBd2KstarMuMu.py")

# Greig's selections
importOptions( "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingBd2JpsiKstar.py")
importOptions( "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingBd2JpsiKS.py")
importOptions( "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingBs2JpsiPhi.py")
importOptions( "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingBu2JpsiK.py")

# Topological selections
importOptions( "$STRIPPINGSELECTIONSROOT/options/STEP09/StrippingTopo.py")
