###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/IsolationTools
-------------------
#]=======================================================================]

gaudi_add_module(IsolationTools
    SOURCES
        src/RelInfoBKstarMuMuBDT.cpp
        src/RelInfoBKstarMuMuHad.cpp
        src/RelInfoBKsttautauMuonIsolationBDT.cpp
        src/RelInfoBKsttautauTauIsolationBDT.cpp
        src/RelInfoBKsttautauTrackIsolationBDT.cpp
        src/RelInfoBs2MuMuBIsolations.cpp
        src/RelInfoBs2MuMuIsolations.cpp
        src/RelInfoBs2MuMuTrackIsolations.cpp
        src/RelInfoBs2MuMuZVisoBDT.cpp
        src/RelInfoBstautauCDFIso.cpp
        src/RelInfoBstautauMuonIsolation.cpp
        src/RelInfoBstautauMuonIsolationBDT.cpp
        src/RelInfoBstautauTauIsolation.cpp
        src/RelInfoBstautauTauIsolationBDT.cpp
        src/RelInfoBstautauTrackIsolation.cpp
        src/RelInfoBstautauTrackIsolationBDT.cpp
        src/RelInfoBstautauZVisoBDT.cpp
        src/RelInfoGammaIso.cpp
        src/RelInfoJetsVariables.cpp
        src/RelInfoMuonIDPlus.cpp
        src/RelInfoMuonIsolation.cpp
        src/RelInfoTrackIsolationBDT.cpp
        src/RelInfoTrackIsolationBDT2.cpp
        src/RelInfoVeloTrackMatch.cpp
        src/RelInfoVertexIsolationBDT.cpp
        src/RelInfoVertexIsolationRadiative.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::CaloUtils
        LHCb::DAQEventLib
        LHCb::HltEvent
        LHCb::LHCbKernel
        LHCb::MuonDetLib
        LHCb::OTDAQLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        LHCb::TrackEvent
        Phys::DaVinciInterfacesLib
        Phys::DaVinciKernelLib
        Phys::DaVinciMCKernelLib
        Phys::DaVinciTypesLib
        Phys::LoKiArrayFunctorsLib
        Phys::LoKiPhysLib
        Phys::MVADictToolsLib
        Rec::MuonIDLib
        ROOT::Core
        ROOT::GenVector
        ROOT::Hist
        ROOT::MathCore
        ROOT::Matrix
        ROOT::Physics
        ROOT::RIO
        ROOT::TMVA
        Stripping::IncTopoVertLib
)

gaudi_add_tests(QMTest)
