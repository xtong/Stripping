/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "RelInfoGammaIso.h"
#include "Event/Particle.h"
#include "Kernel/RelatedInfoNamed.h"
#include <iostream>

using namespace Gaudi;
using namespace LHCb;

//=============================================================================
// Implementation file for class : RelInfoGammaIso
//
// 2017-05-15 : Meril Reboud, Jean-Francois Marchand
//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( RelInfoGammaIso )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RelInfoGammaIso::RelInfoGammaIso( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ), m_PhotonID( 22 ) {
  declareInterface<IRelatedInfoTool>( this );

  declareProperty( "Veto", m_veto = {{"Pi0",
                                      {"Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles",
                                       "Phys/StdLoosePi024e/Particles"}},
                                     {"Eta", {"Phys/StdLooseEta2gg/Particles"}}} );

  m_keys.clear();
  m_keys.push_back( RelatedInfoNamed::GAMMAISONPI0 );
  m_keys.push_back( RelatedInfoNamed::GAMMAISONETA );
  m_keys.push_back( RelatedInfoNamed::GAMMAISODMPI0 );
  m_keys.push_back( RelatedInfoNamed::GAMMAISODMETA );
}

StatusCode RelInfoGammaIso::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  return sc;
}

//=============================================================================

StatusCode RelInfoGammaIso::calculateRelatedInfo( const Particle* top, const Particle* P ) {

  bool test = true;

  if ( msgLevel( MSG::DEBUG ) )
    debug() << " part is " << P->particleID().pid() << " while top is " << top->particleID().pid() << endmsg;

  if ( !P ) return StatusCode::FAILURE;

  if ( P->particleID().pid() == m_PhotonID && isPureNeutralCalo( P ) ) {

    const LHCb::ProtoParticle* proto = P->proto();
    if ( NULL == proto ) return StatusCode::SUCCESS;

    dm_pi0             = 60;  // Mass Window of the Pi02gg container
    dm_eta             = 105; // Mass Window of the eta2gg container
    double dm_pi0_temp = 60;
    double dm_eta_temp = 105;
    double MM          = 0; // Pi0 Measured Mass
    int    n_pi0       = 0;
    int    n_eta       = 0;

    for ( std::map<std::string, std::vector<std::string>>::const_iterator i = m_veto.begin(); m_veto.end() != i; ++i ) {

      const std::string&              flag   = i->first;
      const std::vector<std::string>& cont   = i->second;
      std::vector<std::string>&       m_cont = const_cast<std::vector<std::string>&>( cont );

      // Loop over list of containers
      for ( std::vector<std::string>::iterator i = m_cont.begin(); i != m_cont.end(); ++i ) {

        std::string container = *i;
        if ( container != "" ) {
          if ( exist<LHCb::Particle::Range>( container ) ) {

            LHCb::Particle::Range m_parts;
            m_parts = get<LHCb::Particle::Range>( container );
            int i1  = container.find( "Phys/" );
            int i2  = container.find( "/Particles" );
            if ( i1 != (int)std::string::npos ) i1 += 5;
            if ( i2 != (int)std::string::npos ) i2 -= 12;
            std::string cont = container.substr( i1, i2 );

            for ( LHCb::Particle::Range::const_iterator j = m_parts.begin(); m_parts.end() != j; ++j ) {

              std::vector<const LHCb::Particle*> vp1 = getTree( P );
              std::vector<const LHCb::Particle*> vp2 = getTree( *j );

              for ( std::vector<const LHCb::Particle*>::const_iterator pi = vp1.begin(); vp1.end() != pi; ++pi ) {
                for ( std::vector<const LHCb::Particle*>::const_iterator pj = vp2.begin(); vp2.end() != pj; ++pj ) {

                  const LHCb::Particle*      p1  = *pi;
                  const LHCb::Particle*      p2  = *pj;
                  const LHCb::ProtoParticle* pp1 = p1->proto();
                  const LHCb::ProtoParticle* pp2 = p2->proto();
                  if ( isPureNeutralCalo( p1 ) && isPureNeutralCalo( p2 ) ) {
                    int id1 = (int)pp1->info( LHCb::ProtoParticle::CaloNeutralID, 0 );
                    int id2 = (int)pp2->info( LHCb::ProtoParticle::CaloNeutralID, 0 );
                    if ( id1 == id2 && 0 != id1 ) {
                      MM = ( *j )->momentum().M();
                      if ( ( flag == "Pi0" ) && ( MM > 10 ) ) { // avoid meaningless Pi0
                        n_pi0 += 1;
                        dm_pi0_temp = fabs( MM - 134.9766 ); // Pi0_Mass
                        if ( dm_pi0_temp < dm_pi0 ) { dm_pi0 = dm_pi0_temp; }
                      }
                      if ( ( flag == "Eta" ) && ( MM > 10 ) ) {
                        n_eta += 1;
                        dm_eta_temp = fabs( MM - 547.862 ); // Eta_Mass
                        if ( dm_eta_temp < dm_eta ) { dm_eta = dm_eta_temp; }
                      }
                    }
                  } else if ( !isPureNeutralCalo( p1 ) && !isPureNeutralCalo( p2 ) ) {
                    if ( pp1 == pp2 ) {
                      MM = ( *j )->momentum().M();
                      if ( ( flag == "Pi0" ) && ( MM > 10 ) ) {
                        n_pi0 += 1;
                        dm_pi0_temp = fabs( MM - 134.9766 ); // Pi0_Mass
                        if ( dm_pi0_temp < dm_pi0 ) { dm_pi0 = dm_pi0_temp; }
                      }
                      if ( ( flag == "Eta" ) && ( MM > 10 ) ) {
                        n_eta += 1;
                        dm_eta_temp = fabs( MM - 547.862 ); // Eta_Mass
                        if ( dm_eta_temp < dm_eta ) { dm_eta = dm_eta_temp; }
                      }
                    }
                  } else if ( isPureNeutralCalo( p1 ) && !isPureNeutralCalo( p2 ) ) {
                    if ( ( pp2->calo() ).empty() ) continue;
                    int id1 = (int)pp1->info( LHCb::ProtoParticle::CaloNeutralID, 0 );
                    int id2 = (int)pp2->info( LHCb::ProtoParticle::CaloNeutralID, 0 );
                    if ( id1 == id2 && 0 != id1 ) {
                      MM = ( *j )->momentum().M();
                      if ( ( flag == "Pi0" ) && ( MM > 10 ) ) {
                        n_pi0 += 1;
                        dm_pi0_temp = fabs( MM - 134.9766 ); // Pi0_Mass
                        if ( dm_pi0_temp < dm_pi0 ) { dm_pi0 = dm_pi0_temp; }
                      }
                      if ( ( flag == "Eta" ) && ( MM > 10 ) ) {
                        n_eta += 1;
                        dm_eta_temp = fabs( MM - 547.862 ); // Eta_Mass
                        if ( dm_eta_temp < dm_eta ) { dm_eta = dm_eta_temp; }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    n_pi0_f = (float)n_pi0;
    n_eta_f = (float)n_eta;

    m_map.clear();

    std::vector<short int>::const_iterator ikey;
    for ( ikey = m_keys.begin(); ikey != m_keys.end(); ikey++ ) {

      float value = 0;
      switch ( *ikey ) {

      case RelatedInfoNamed::GAMMAISONPI0:
        value = n_pi0_f;
        break;
      case RelatedInfoNamed::GAMMAISONETA:
        value = n_eta_f;
        break;
      case RelatedInfoNamed::GAMMAISODMPI0:
        value = dm_pi0;
        break;
      case RelatedInfoNamed::GAMMAISODMETA:
        value = dm_eta;
        break;
      }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "  Inserting key = " << *ikey << ", value = " << value << " into map" << endmsg;

      m_map.insert( std::make_pair( *ikey, value ) );
    }
  }

  return StatusCode( test );
}

// rel info methods

LHCb::RelatedInfoMap* RelInfoGammaIso::getInfo( void ) { return &m_map; }

std::vector<const LHCb::Particle*> RelInfoGammaIso::getTree( const LHCb::Particle* P ) {
  std::vector<const LHCb::Particle*> tree;
  if ( P->proto() ) {
    tree.push_back( P );
    return tree;
  } else {
    const LHCb::Particle::ConstVector& daughters = P->daughtersVector();
    for ( LHCb::Particle::ConstVector::const_iterator d = daughters.begin(); daughters.end() != d; ++d ) {
      if ( ( *d )->proto() )
        tree.push_back( *d );
      else {
        const std::vector<const LHCb::Particle*>& t = getTree( *d );
        for ( std::vector<const LHCb::Particle*>::const_iterator i = t.begin(); t.end() != i; ++i ) {
          tree.push_back( *i );
        }
      }
    }
  }
  return tree;
}
