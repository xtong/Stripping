/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RELINFOTRACKISOVARIABLESVSLL_H
#define RELINFOTRACKISOVARIABLESVSLL_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Event/RelatedInfoMap.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/GetIDVAlgorithm.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IRelatedInfoTool.h"
// needed for TMVA
#include "Event/Particle.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IParticleDictTool.h"
#include "MVADictTools/TMVATransform.h"

/** @class RelInfoBKstarMuMuBDT RelInfoBKstarMuMuBDT.h
 *
 * \brief Calculate track isolation.
 *    Compare daughter
 *
 *    m_bdt1 : bdt value
 *
 * Options:
 *
 *   None
 *
 *  Converted from ConeVariables by A. Shires 18/06/2014
 *
 *  @author Giampiero Mancinelli
 *  @date   2014-07-25
 *
 */

struct IDVAlgorithm;
struct IDistanceCalculator;
class IParticleDictTool;

class RelInfoBKstarMuMuBDT : public GaudiTool, virtual public IRelatedInfoTool {
public:
  /// Standard constructor
  RelInfoBKstarMuMuBDT( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode calculateRelatedInfo( const LHCb::Particle*, const LHCb::Particle* ) override;

  LHCb::RelatedInfoMap* getInfo( void ) override;

  virtual std::string infoPath( void );

  virtual ~RelInfoBKstarMuMuBDT(); ///< Destructor

private:
  std::vector<std::string> m_variables;
  LHCb::RelatedInfoMap     m_map;
  std::vector<short int>   m_keys;

  double                     m_bdt1;
  double                     m_bdt2;
  int                        is_kst;
  const IDistanceCalculator* m_dist;
  double                     m_count_mum, m_count_mup, m_count_mum_f, m_count_mup_f;
  IDVAlgorithm*              m_dva;
  std::string                m_ParticlePath;
  int                        m_tracktype;

  // variables for BDT:
  float tv_angle, tv_fc, tv_doca, tv_svdis, tv_pvdis, tv_pt, tv_ips, tv_ip, tv_trkchi2;

  StatusCode          fillIsolation( const LHCb::Particle* top );
  std::vector<double> getIso( const LHCb::Particle* B );
  double              ratio( double p1, double p2 );

  double getphi( const LHCb::Particle* vdau1, const LHCb::Particle* vdau2 );
  double gettheta( const LHCb::Particle* vdau1, const LHCb::Particle* vdau2 );
  double IsClose( const LHCb::Particle* p1, const LHCb::Particle* p2 );
  void   closest_point( Gaudi::XYZPoint o, Gaudi::XYZVector p, Gaudi::XYZPoint o_mu, Gaudi::XYZVector p_mu,
                        Gaudi::XYZPoint& close1, Gaudi::XYZPoint& close2, Gaudi::XYZPoint& vertex, bool& fail );

  double arcosine( Gaudi::XYZVector p1, Gaudi::XYZVector p2 );
  void   InCone( Gaudi::XYZPoint o1, Gaudi::XYZVector p1, Gaudi::XYZPoint o2, Gaudi::XYZVector p2, Gaudi::XYZPoint& vtx,
                 double& doca, double& angle );

  void IsHltGood( Gaudi::XYZPoint o, Gaudi::XYZVector p, Gaudi::XYZPoint o_mu, Gaudi::XYZVector p_mu,
                  Gaudi::XYZPoint PV, bool& hltgood, double& fc );

  double pointer( Gaudi::XYZVector vertex, Gaudi::XYZVector p, Gaudi::XYZVector p_mu );

  std::string m_weightsName;
  /// TMVA transform
  // Reader
  // TMVA::Reader *m_Reader;
  std::string m_transformName;
  // variables
  double m_angle, m_fc, m_doca_iso, m_ips, m_svdis, m_svdis_h, m_pvdis, m_pvdis_h;

  IParticleDescendants*   m_descend;
  TMVATransform           m_tmva;
  TMVATransform::optmap   m_optmap;
  IParticleDictTool::DICT m_varmap;
  IParticleDictTool::DICT m_out;

  // save the vertice
  std::string m_PVInputLocation;

  std::vector<const LHCb::Particle*> m_decayParticles;

  /// Save all particles in your decay descriptor in a vector
  void saveDecayParticles( const LHCb::Particle* top );

  /// Check if your track belongs to your decay or not
  bool isTrackInDecay( const LHCb::Track* track );

  ///============================================================================
  /// Track isolation method
  ///============================================================================
  /*
        bool calcBDTValue( const LHCb::Particle * part
                , const LHCb::Particles * tracks
                , const LHCb::VertexBase * PV
                , const LHCb::VertexBase * SV
                ) ;
  */
};

#endif // CONEVARIABLES_H
