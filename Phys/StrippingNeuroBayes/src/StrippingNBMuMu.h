/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef STRIPPINGNBMUMU_H
#define STRIPPINGNBMUMU_H 1

#include "Kernel/DaVinciHistoAlgorithm.h"
#include "Kernel/FPEGuard.h"

#include "math.h"
#include <sstream>
#include <string>

// NeuroBayes (only if available)
#ifdef HAVE_NEUROBAYES
#  include "NeuroBayesExpert.hh"
#  include "nb_param.hh"
#endif

/** @class StrippingNBMuMu StrippingNBMuMu.h
 *
 *
 *  @author Ulrich Kerzel
 *  @date   2010-10-15
 */
class StrippingNBMuMu : public DaVinciHistoAlgorithm {

public:
  /// Standard constructor
  StrippingNBMuMu( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~StrippingNBMuMu(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
#ifdef HAVE_NEUROBAYES
  StatusCode getInputVar( const LHCb::Particle& particle );
#endif
  double minIPChi2( const LHCb::Particle& particle );

  double m_NetworkCut;
  bool   m_PlotHisto;

  double m_PlotMassMin;
  double m_PlotMassMax;
  int    m_PlotNBins;

#ifdef HAVE_NEUROBAYES
  Expert* m_NBExpert;
  float*  m_inArray;
#endif

  std::string m_ExpertiseName;
  std::string m_netVersion;
  std::string m_pvLocation;
};

#endif // STRIPPINGNBMUMU_H
