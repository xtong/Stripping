/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DISPLVERTICES_HLTVERTEXCONVERTERS20p3_H
#define DISPLVERTICES_HLTVERTEXCONVERTERS20p3_H 1

#include "Kernel/DaVinciAlgorithm.h"

// fwd
namespace LHCb {
  class HltObjectSummary;
}

/** @class HltVertexConverterS20p3 HltVertexConverterS20p3.h DisplVertices/HltVertexConverterS20p3.h
 *
 * Revive Hlt2 candidate vertices
 * @see HltTrackConverter
 *
 * @author Pieter David
 * @date   2013-12-29
 */
class HltVertexConverterS20p3 : public DaVinciAlgorithm {
public:
  /// Standard constructor
  HltVertexConverterS20p3( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~HltVertexConverterS20p3(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization
private:
  std::string              m_HltSelReportsLocation;
  std::vector<std::string> m_HltLineNames;

  bool                 m_recursive;
  bool                 m_matchTracksToOffline;
  Gaudi::SymMatrix3x3  m_defaultVertexCovMatrix;
  LHCb::Particle*      reviveParticle( const LHCb::HltObjectSummary* summary, bool recursive );
  LHCb::ProtoParticle* reviveProto( const LHCb::HltObjectSummary* summary );
  LHCb::Track*         reviveTrack( const LHCb::HltObjectSummary* summary );

  LHCb::ProtoParticles* m_protos;
  LHCb::Tracks*         m_tracks;
  LHCb::ProtoParticles* protoParticles();
  LHCb::Tracks*         tracks();
};
#endif // DISPLVERTICES_HLTVERTEXCONVERTERS20p3_H
