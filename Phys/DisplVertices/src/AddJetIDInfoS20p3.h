/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DISPLVERTICES_ADDJETIDINFOS20P3_H
#define DISPLVERTICES_ADDJETIDINFOS20P3_H 1

#include <boost/foreach.hpp>

#include "GaudiAlg/GaudiTool.h"

#include "Kernel/IExtraInfoTool.h"

#include "Kernel/JetEnums.h"
#include "Kernel/PFParticle.h"

#include "LoKi/PhysTypes.h"

/**
 * IExtraInfoTool to fill JetID variables, Stripping20p3 version
 *
 * @author Victor Coco
 * @author Pieter David
 * @date   2013-12-19
 */
class AddJetIDInfoS20p3 : public GaudiTool, virtual public IExtraInfoTool {
public:
  AddJetIDInfoS20p3( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;
  StatusCode finalize() override;

  // interface methods
  StatusCode calculateExtraInfo( const LHCb::Particle* top, const LHCb::Particle* part ) override;
  int        getFirstIndex( void ) override { return LHCb::JetIDInfo::StartJetIDInfo + 1; }
  int        getNumberOfParameters( void ) override { return LHCb::JetIDInfo::MNF - LHCb::JetIDInfo::StartJetIDInfo; }
  int        getInfo( int index, double& value, std::string& name ) override;

private:
  // const versions of the above

  std::vector<double> m_cache;
  inline double       getCache( int key ) { return m_cache[key - getFirstIndex()]; }
  inline void         setCache( int key, const double& value ) { m_cache[key - getFirstIndex()] = value; }
  inline void         clearCache() { m_cache = std::vector<double>( getNumberOfParameters(), -10. ); }
  bool                cacheValid() const;
  void                dump();

  // functors
  LoKi::PhysTypes::Fun PFTYPE;
  LoKi::PhysTypes::Fun NSATECAL;
  LoKi::PhysTypes::Fun NSATHCAL;
  LoKi::PhysTypes::Fun NWITHPVINFO;
  LoKi::PhysTypes::Fun NWITHPVINFOCHI24;
  LoKi::PhysTypes::Cut HASPVINFO;
};
#endif // DISPLVERTICES_ADDJETIDINFOS20P3_H
