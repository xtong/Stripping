/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DISPLVERTICES_PERPVOFFSETJETS20P3_H
#define DISPLVERTICES_PERPVOFFSETJETS20P3_H 1

#include "GaudiAlg/GaudiTool.h"

#include "Kernel/IParticleReFitter.h"

class TH3D;

/**
 * Jet Energy Correction for Displaced Vertex jets in Stripping20r*p3
 *
 * @author Victor Coco
 * @author Pieter David
 * @date   2013-12-19
 */
class PerPVOffsetJECS20p3 : public GaudiTool, virtual public IParticleReFitter {
public:
  PerPVOffsetJECS20p3( const std::string& type, const std::string& name, const IInterface* parent );

  ~PerPVOffsetJECS20p3();

  StatusCode initialize() override;

  // interface methods
  StatusCode reFit( LHCb::Particle& jet ) const override;

private:
  bool               m_apply;
  std::string        m_histo_path;
  std::vector<TH3D*> m_histos;
  float              m_shiftJEC;
};
#endif // DISPLVERTICES_PERPVOFFSETJETS20P3_H
