###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Utilities for StrippingConf modules.
'''

__author__ = 'Michael Alexander <michael.thomas.alexander@cern.ch>'

from Gaudi.Configuration import log

def _limitCombinatoricsMaxCandidates(configurable, MaxCandidates,
                                     MaxCombinations, force, incidentName) :
    '''Limit combinatorics of a configurable assuming it has MaxCandidates,
    StopAtMaxCandidates, MaxCombinations, StopAtMaxCombinations and StopIncidentType attributes.'''

    didsomething = False
    if (force or not configurable.isPropertySet('MaxCandidates')) and None != MaxCandidates :
        log.debug( "Forcing MaxCandidates settings for " + configurable.name() )
        configurable.MaxCandidates = MaxCandidates
        configurable.StopAtMaxCandidates = True
        didsomething = True
    if (force or not configurable.isPropertySet('MaxCombinations')) and None != MaxCombinations :
        log.debug( "Forcing MaxCombinations settings for " + configurable.name() )
        configurable.MaxCombinations = MaxCombinations
        configurable.StopAtMaxCombinations = True
        didsomething = True
    if didsomething :
        configurable.StopIncidentType = incidentName
    return didsomething 

def _limitCombinatoricsMaxParticles(configurable, MaxCandidates, force, incidentName) :
    '''Limit combinatorics for a configurable assuming it's got MaxParticles & StopIncidentType
    attributes.'''

    if (force or not configurable.isPropertySet('MaxParticles')) and None != MaxCandidates :
        log.debug( "Forcing MaxParticles settings for " + configurable.name() )
        configurable.MaxParticles = MaxCandidates
        configurable.StopIncidentType = incidentName
        return True
    return False

## List of Configurables that will fail-fast on limitCombinatorics
# Added by hand.
WHITELIST = (
    'AddExtraInfo',
    'AddRelatedInfo',
    'bJetSeeds',
    'CheckPV',
    'ConjugateNeutralPID',
    'DiElectronMaker',
    'DisplacedVertexJetCandidateMakerS20p3',
    'FilterDesktop',
    'HltVertexConverterS20p3',
    'LoKi__HDRFilter',
    'LoKi__VoidFilter',
    'LLParticlesFromRecVertices',
    'OfflineRateLimiter',
    'PatPV3D',
    'Pi0Veto__Tagger',
    'ProcStatusCheck',
    'SelectVeloTracksNotFromPVS20p3',
    'TisTosParticleTagger',
    'TopoTauAlg',
    'VeloEventShapeCutsS20p3',
)

        
def limitCombinatorics( configurable,
                        MaxCandidates,
                        MaxCombinations,
                        force,
                        incidentName = 'ExceedsCombinatoricsLimit' ) :
    """
    - This function has the highest call counts. So it needs careful optimization.
    - Because the action is reactive, it's best to order the case of most-probable
      to the least-probable to exit the check as early as possible.
    """

    ## Check the name by string is MUCH FASTER than by class.
    classname = configurable.__class__.__name__
    
    ## Early abort for these class which doesn't need limitation.
    if classname in WHITELIST:
        return False

    ## Early dealing with nested
    if classname == 'GaudiSequencer':
        val = False
        for conf in configurable.Members:
            val |= limitCombinatorics( conf, MaxCandidates, MaxCombinations, force, incidentName )
        return val

    if classname in ('CombineParticles','DaVinci__N3BodyDecays','DaVinci__N4BodyDecays', 'DaVinci__N5BodyDecays'):        
        return _limitCombinatoricsMaxCandidates(configurable, MaxCandidates, MaxCombinations, force, incidentName)

    if classname == 'StrippingAlg':
        val = False
        stages = [ 'Filter1' ]
        for conf in [ getattr( configurable, stage ) for stage in stages if hasattr(configurable, stage) ] :
            val |= limitCombinatorics( conf, MaxCandidates, MaxCombinations, force, incidentName )
        if val: 
            configurable.IncidentsToBeFlagged += [ incidentName ]
        return val

    if classname in ('SubPIDMMFilter', 'SubstitutePID'):
        return _limitCombinatoricsMaxParticles(configurable, MaxCandidates, force, incidentName)

    ## Generic handling (slow)
    if hasattr(type(configurable),'StopAtMaxCandidates') and hasattr(type(configurable),'MaxCandidates') and \
            hasattr(type(configurable),'StopAtMaxCombinations') and hasattr(type(configurable),'MaxCombinations'):
        return _limitCombinatoricsMaxCandidates(configurable, MaxCandidates, MaxCombinations, force, incidentName)

    if hasattr(configurable, 'MaxParticles') and hasattr(configurable, 'StopIncidentType') :
        return _limitCombinatoricsMaxParticles(configurable, MaxCandidates, force, incidentName)        

    ## Generic handling (slow)
    if hasattr( configurable, 'Members' ) :
        #print classname
        val = False
        for i in getattr( configurable, 'Members' ) :
            val |= limitCombinatorics( i, MaxCandidates, MaxCombinations, force, incidentName )
        return val

    #log.warning('Not able to set MaxCandidates and MaxCombinations to algorithm '+str(type(configurable))+'/'+configurable.name())
    return False

