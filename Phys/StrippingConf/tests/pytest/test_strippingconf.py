#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
 Test suite for StrippingConf configuration
'''
__author__ = 'Juan Palacios juan.palacios@cern.ch'

from py.test import raises

from StrippingConf.Configuration import StrippingLine, StrippingStream, StrippingConf

from Configurables import FilterDesktop
"""
def test_dumplicated_outputLocation_in_single_stream_raises_Exception() :

    filterX = FilterDesktop('FilterX')

    lineX0 = StrippingLine('LineX0', algos = [filterX])

    lineX1 = StrippingLine('LineX1', algos = [filterX])

    filterY = FilterDesktop('FilterY')

    lineY0 = StrippingLine('LineY0', algos = [filterY])

    lineY1 = StrippingLine('LineY1', algos = [filterY])

    stream = StrippingStream('TestStream', Lines = [lineX0, lineX1, lineY0, lineY1])

    raises( Exception, StrippingConf, Streams = [stream] )
"""


def test_dumplicated_outputLocation_in_different_streams_raises_Exception():

    filter = FilterDesktop('FilterX')

    line0 = StrippingLine('Line0b', algos=[filter])

    line1 = StrippingLine('Line1b', algos=[filter])

    stream0 = StrippingStream('Stream0', Lines=[line0])

    stream1 = StrippingStream('Stream0', Lines=[line1])

    raises(Exception, StrippingConf, Streams=[stream0, stream1])
