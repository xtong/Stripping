###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
DaVinciVersion = 'v28r3p1'
datatype = '2011'
platform = 'x86_64-slc5-gcc43-opt'
stepid = 14138
CondDBtag = 'head-20110622-Reco10'
DDDBtag = 'head-20110302'
TCK = 0
description = ''

# Can't currently get the line info.
lines = {}
