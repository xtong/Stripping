/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BD2EEKSTARBDTSELECTION_H
#define BD2EEKSTARBDTSELECTION_H 1

#include <vector>
// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "Kernel/IParticleFilter.h"

#include "TMVA/Reader.h"
#include "TString.h"

#include "LoKi/Child.h"
#include "LoKi/LoKiPhys.h"
#include "LoKi/ParticleContextCuts.h"
#include "LoKi/ParticleCuts.h"

struct IDistanceCalculator;
class DVAlgorithm;

/** @class Bd2eeKstarBDTSelection Bd2eeKstarBDTSelection.h
 *
 *
 *  @author Jibo He
 *  @date   2012-01-19
 */
class Bd2eeKstarBDTSelection : public extends1<GaudiTool, IParticleFilter> {
public:
  /** initialize tool */
  StatusCode initialize() override;

  /** finalize tool */
  StatusCode finalize() override;

  /**
   *  @see IParticleFilter
   */
  bool operator()( const LHCb::Particle* p ) const override;

  /// Standard constructor
  Bd2eeKstarBDTSelection( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~Bd2eeKstarBDTSelection(); ///< Destructor

private:
  bool set( const LHCb::Particle* p ) const;

  const IDistanceCalculator* m_dist;
  IDVAlgorithm*              m_dva;
  TMVA::Reader*              m_BDTReader;

  unsigned int m_nVars;
  float*       m_values;

  double      m_cut;         // BDT cut value
  std::string m_weightsFile; // weights file

  const LoKi::Cuts::CHI2IP m_BPVIPCHI2;
  const LoKi::Cuts::VDCHI2 m_BPVVDCHI2;
  const LoKi::Cuts::DIRA   m_BPVDIRA;
};
#endif // BD2EEKSTARBDTSELECTION_H
