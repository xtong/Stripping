###############################################################################
# (c) Copyright 2018-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Locate NeuroBayesExpert library
# Defines:
#  Variables:
#    NEUROBAYESEXPERT_FOUND
#    NEUROBAYESEXPERT_INCLUDE_DIR
#    NEUROBAYESEXPERT_INCLUDE_DIRS (not cached)
#    NEUROBAYESEXPERT_LIBRARIES
#    NEUROBAYESEXPERT_LIBRARY_DIRS (not cached)
#  Targets:
#    NeuroBayes::Expert

find_path(NEUROBAYESEXPERT_INCLUDE_DIR NeuroBayesExpert.hh)
find_library(NEUROBAYESEXPERT_LIBRARIES NAMES NeuroBayesExpertCPP)

set(NEUROBAYESEXPERT_INCLUDE_DIRS ${NEUROBAYESEXPERT_INCLUDE_DIR})
get_filename_component(NEUROBAYESEXPERT_LIBRARY_DIRS ${NEUROBAYESEXPERT_LIBRARIES} PATH)

# handle the QUIETLY and REQUIRED arguments and set NEUROBAYESEXPERT_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(NeuroBayesExpert DEFAULT_MSG NEUROBAYESEXPERT_INCLUDE_DIR NEUROBAYESEXPERT_LIBRARIES)

mark_as_advanced(NEUROBAYESEXPERT_FOUND NEUROBAYESEXPERT_INCLUDE_DIR NEUROBAYESEXPERT_LIBRARIES)

if(NEUROBAYESEXPERT_FOUND AND NOT TARGET NeuroBayes::Expert)
    add_library(NeuroBayes::Expert UNKNOWN IMPORTED)
    set_target_properties(NeuroBayes::Expert
        PROPERTIES IMPORTED_LOCATION ${NEUROBAYESEXPERT_LIBRARIES})
    target_include_directories(NeuroBayes::Expert
        INTERFACE ${NEUROBAYESEXPERT_INCLUDE_DIR})
endif()
