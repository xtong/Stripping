2021-07-04 Stripping v14r4p7
===

This version uses Phys v26r8.

This version is released on `run2-patches` branch and is intended for use with Run1 or Run2 data. For Run3, use a version released on `master` branch

Built relative to Stripping [v14r4p6](../-/tags/v14r4p6), with the following changes:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Fix unused variable warnings in case Neurobayes is absent, !1612 (@cattanem)
- Fix or ignore untested StatusCodes exposed by Gaudi v36 and clang11, !1611 (@cattanem)
- Fixes for LCG 99 (run2-patches), !1562 (@masmith)

