2021-11-30 Stripping v16r0
===

This version uses Phys v27r0.

This version is released on `run2-patches` branch and is intended for use with Run1 or Run2 data. For Run3, use a version released on `master` branch
Built relative to Stripping [v14r4p7](../-/tags/v14r4p7), with the following changes:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | (run2) Rewrite CMake configuration in "modern CMake", !1677 (@clemenci) [LBCOMP-23]


