2022-05-20 Stripping v16r2
===

This version uses Phys v27r2.

This version is released on `run2-patches` branch and is intended for use with Run1 or Run2 data. For Run3, use a version released on `master` branch
Built relative to Stripping [v16r1](../-/tags/v16r1), with only changes to upstream packages.

